class User < ActiveRecord::Base
  has_many :owned_issues, class_name: 'Issue', foreign_key: 'owner_id'
  has_many :assigned_issues, class_name: 'Issue', foreign_key: 'assignee_id'

  validates :email, presence: true, uniqueness: true
  validates :name, presence: true
  validates :password_digest, presence: true
end
