class Issue < ActiveRecord::Base
  belongs_to :domain
  belongs_to :status
  belongs_to :priority
  belongs_to :owner, class_name: 'User'
  belongs_to :assignee, class_name: 'User'

  validates :priority, presence: true
  validates :domain, presence: true
  validates :status, presence: true
  validates :owner, presence: true

  scope :text_search, -> (q) { q.present? ? basic_search(q) : all }
  scope :by_priority, -> (p) { p.present? ? where(priority: p) : all }
  scope :by_owner, -> (o) { o.present? ? where(owner_id: o) : all }
  scope :by_assignee, -> (a) { a.present? ? where(assignee_id: a) : all }
end
