class IssuesController < ApplicationController
  before_action :require_login, raise: false

  def index
    @issues = Issue.text_search(params[:query])
                   .by_priority(params[:priority])
                   .by_owner(params[:owner])
                   .by_assignee(params[:assignee])
  end

  def show
    @issue = Issue.find(params[:id])
  end

  def new
    @issue = current_user.owned_issues.new
  end

  def create
    @issue = current_user.owned_issues.new(issue_params)
    if @issue.save
      redirect_to @issue
    else
      render 'new'
    end
  end

  def edit
    @issue = Issue.find(params[:id])
  end

  def update
    @issue = Issue.find(params[:id])
    if @issue.update_attributes(issue_params)
      redirect_to @issue
    else
      render 'edit'
    end
  end

  def destroy
    @issue = Issue.find(params[:id])
    @issue.destroy
    redirect_to issues_path
  end

  def uat
    @priorities = Priority.all
    @statuses = Status.all
    @issues = Issue.all
  end

  private

  def issue_params
    params.require(:issue).permit(
      :observed_behaviour, :expected_behaviour, :priority_id, :domain_id,
      :assignee_id, :steps_to_reproduce, :additional_information, :status_id,
      :jira_reference
    )
  end
end
