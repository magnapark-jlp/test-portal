class AddOwnerToIssues < ActiveRecord::Migration
  def change
    add_column :issues, :owner_id, :integer
    add_index :issues, :owner_id
  end
end
