class AddAssigneeIdToIssues < ActiveRecord::Migration
  def change
    add_column :issues, :assignee_id, :integer
    add_index :issues, :assignee_id
  end
end
