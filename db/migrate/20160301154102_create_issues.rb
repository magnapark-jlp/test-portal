class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.text :observed_behaviour
      t.text :expected_behaviour
      t.string :priority

      t.timestamps null: false
    end
  end
end
