class AddFieldsToIssues < ActiveRecord::Migration
  def change
    add_column :issues, :steps_to_reproduce, :text
    add_column :issues, :additional_information, :text
    add_column :issues, :status_id, :integer
    add_column :issues, :jira_reference, :string
    add_index :issues, :status_id
  end
end
