class AddPriorityIdToIssues < ActiveRecord::Migration
  def change
    add_column :issues, :priority_id, :integer
    add_index :issues, :priority_id
  end
end
