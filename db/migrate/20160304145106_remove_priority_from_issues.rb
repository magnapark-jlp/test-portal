class RemovePriorityFromIssues < ActiveRecord::Migration
  def change
    remove_column :issues, :priority
  end
end
