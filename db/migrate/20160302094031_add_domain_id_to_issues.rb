class AddDomainIdToIssues < ActiveRecord::Migration
  def change
    add_column :issues, :domain_id, :integer
    add_index :issues, :domain_id
  end
end
