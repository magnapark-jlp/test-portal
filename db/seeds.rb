statuses = ['New', 'Open', 'To Discuss', 'Develop', 'Deployed', 'Closed']

statuses.each do |status|
  Status.find_or_create_by name: status
end

priorities = ['Cosemetic', 'Low', 'Medium', 'High', 'Very High']

priorities.each do |priority|
  Priority.find_or_create_by name: priority
end
