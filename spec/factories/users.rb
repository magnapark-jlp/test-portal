FactoryGirl.define do
  factory :user do
    email 'nick.bayley@johnlewis.co.uk'
    name 'Nick Bayley'
    password_digest 'password'
  end
end
