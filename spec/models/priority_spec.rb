require 'rails_helper'

RSpec.describe Priority, type: :model do
  it 'has valid factories' do
    expect(build(:priority)).to be_valid
  end

  let(:priority) { build :priority }

  describe 'ActiveModel validations' do
    subject { priority }
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name) }
  end

  describe 'ActiveRecord associations' do
    it { is_expected.to have_many(:issues) }
  end
end
