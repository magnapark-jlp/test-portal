require 'rails_helper'

RSpec.describe Domain, type: :model do
  it 'has valid factories' do
    expect(build(:domain)).to be_valid
  end

  let(:domain) { build :domain }

  describe 'ActiveModel validations' do
    subject { domain }
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name) }
  end

  describe 'ActiveRecord associations' do
    it { is_expected.to have_many(:issues) }
  end
end
