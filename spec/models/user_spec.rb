require 'rails_helper'

RSpec.describe User, type: :model do
  it 'has valid factories' do
    expect(build(:user)).to be_valid
  end

  let(:user) { build :user }

  describe 'ActiveModel validations' do
    subject { user }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_uniqueness_of(:email) }
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:password_digest) }
  end

  describe 'ActiveRecord associations' do
    it do
      is_expected.to have_many(:owned_issues)
        .class_name('Issue').with_foreign_key('owner_id')
    end
    it do
      is_expected.to have_many(:assigned_issues)
        .class_name('Issue').with_foreign_key('assignee_id')
    end
  end

  # describe '#full_name' do
  #   it 'returns the users full name as a string' do
  #     user.assign_attributes forename: 'Joe', surname: 'Bloggs'
  #     expect(user.full_name).to eq 'Joe Bloggs'
  #   end
  # end
end
