require 'rails_helper'

RSpec.describe Issue, type: :model do
  describe 'ActiveModel validations' do
    it { is_expected.to validate_presence_of(:domain) }
    it { is_expected.to validate_presence_of(:status) }
    it { is_expected.to validate_presence_of(:priority) }
    it { is_expected.to validate_presence_of(:owner) }
  end

  describe 'ActiveRecord associations' do
    it { is_expected.to belong_to(:domain) }
    it { is_expected.to belong_to(:status) }
    it { is_expected.to belong_to(:priority) }
    it { is_expected.to belong_to(:owner).class_name('User') }
    it { is_expected.to belong_to(:assignee).class_name('User') }
  end
end
